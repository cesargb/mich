package main

import (
	"fmt"
	"database/sql"
	_ "github.com/lib/pq"
)


type DbSchema struct {
	Name   string
	Tables []TableSquema
}

type TableSquema struct {
	Name    string
	Columns []ColumnSquema
	HasMany []RelationSquema
	BelongsTo []RelationSquema
}



type RelationSquema struct {
	SourceTable string
	SourceColumn string //id
	TargetTable string //car types
	TargetColumn string //company_id
}

type ColumnSquema struct {
	Name string
	ColumnDefault *string
	IsNullableStr string
	isNullable bool
	DataType string
	CharacterMaximumLength *string
}

var queryTables = "SELECT table_name " +
	"FROM information_schema.Tables "+
	"WHERE table_type = 'BASE TABLE' "+
	"AND table_schema NOT IN "+
	"('pg_catalog', 'information_schema') "+
	"AND table_catalog= $1 "

var queryColumns ="SELECT column_name, column_default,is_nullable,data_type,character_maximum_length "+
	"FROM information_schema.Columns "+
	"WHERE table_name = $1 "
var queryRel = `
		SELECT source_table::regclass, source_attr.attname AS source_column,
			target_table::regclass, target_attr.attname AS target_column
		FROM pg_attribute target_attr, pg_attribute source_attr,
			(SELECT source_table, target_table, source_constraints[i] source_constraints, target_constraints[i] AS target_constraints
			FROM
				(SELECT conrelid as source_table, confrelid AS target_table, conkey AS source_constraints, confkey AS target_constraints,
					generate_series(1, array_upper(conkey, 1)) AS i
				FROM pg_constraint
				WHERE contype = 'f'
				) query1
			) query2
		WHERE target_attr.attnum = target_constraints AND target_attr.attrelid = target_table AND
			  source_attr.attnum = source_constraints AND source_attr.attrelid = source_table;
	`//


func DBSync(url string) (*DbSchema,error){
	user:= "test-user"
	database:="wxrdb"
	db, err := sql.Open("postgres", fmt.Sprint("user=", user, " dbname=", database, " sslmode=disable"))
	if err != nil {
		return nil,err
	}
	dbs:= &DbSchema{Name:database, Tables:make([]TableSquema,0)}
	tables,err := db.Query(queryTables,database)
	if err!=nil{return nil,err}
	defer  tables.Close()
	for tables.Next() {
		t := &TableSquema{Columns: make([]ColumnSquema, 0)}
		err := tables.Scan(&t.Name)
		if err!=nil{return nil,err}
		cols, err := db.Query(queryColumns, t.Name)
		if err!=nil{return nil,err}
		defer cols.Close()
		for cols.Next() {
			c := &ColumnSquema{}
			err := cols.Scan(&c.Name, &c.ColumnDefault, &c.IsNullableStr, &c.DataType, &c.CharacterMaximumLength)
			if err!=nil{return nil,err}
			c.isNullable = c.IsNullableStr == "YES"
			t.Columns = append(t.Columns, *c)
		}
		dbs.Tables = append(dbs.Tables,*t)
	}
	//relations

	relations,err := db.Query(queryRel)
	if err!=nil{return nil,err}
	defer  relations.Close()
	for relations.Next() {
		r := &RelationSquema{}
		err := relations.Scan(&r.SourceTable,&r.SourceColumn,&r.TargetTable,&r.TargetColumn)
		if err!=nil{return nil,err}
		for i,s := range dbs.Tables {
			if s.Name == r.SourceTable {
				dbs.Tables[i].BelongsTo = append(dbs.Tables[i].BelongsTo,*r)
			}
		}
		for i,s := range dbs.Tables {
			if s.Name == r.TargetTable {
				dbs.Tables[i].HasMany = append(dbs.Tables[i].HasMany,*r)
			}
		}


	}
	return dbs,nil
}
