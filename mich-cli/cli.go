package main

import (
	"fmt"
	"os"
	"flag"
)
var appName="mich-cli"

func main() {
	// Execution of CLI tool behavior goes here

	fmt.Println("mich-cli")

	// Subcommands
	initCommand := flag.NewFlagSet("init", flag.ExitOnError)
	initUrlFlag := initCommand.String("url","./","directory of the application [./]")

	genCommand := flag.NewFlagSet("gen", flag.ExitOnError)
	genFileUrlFlag := genCommand.String("file","./gen.json","config file url for gen command [./gen.json]")
	genDbFileUrlFlag := genCommand.String("dbfile","./dbschema.json","db schema file url for gen command [./dbschema.json]")
	if len(os.Args) == 1 {
		fmt.Println("usage:", appName, "<command> [<args>]")
		return
	}

	switch os.Args[1] {
	case "init":
		initCommand.Parse(os.Args[2:])
	case "gen":
		genCommand.Parse(os.Args[2:])
	default:
		fmt.Println("subcommand",os.Args[1],"is not valid")
		os.Exit(1)
	}
	if initCommand.Parsed(){
			os.Exit(InitCmd(*initUrlFlag))
	}else if genCommand.Parsed() {
		os.Exit(GenCmd(*genFileUrlFlag,*genDbFileUrlFlag))
	}



}





