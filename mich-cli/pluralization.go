package main

import "regexp"

type RuleGroup struct {
	PluralRules []Rule
	SingularRules []Rule
	IrregularRules []Rule
}

type Rule struct {
	From string
	To string
}

var rules  = map[string]RuleGroup{
	"en":RuleGroup{
		PluralRules: []Rule{
			Rule{`(.+)(ies$)`,"${1}y"},
			Rule{`(.+)(s$)`,`$1`},
		},
	},
}

type  PluralUtil struct {
	RuleGroup RuleGroup
}

func NewPluralUtil(lang string) PluralUtil {
	r,ok:= rules[lang]
	if !ok {
		panic("rule group not exist, "+lang)
	}
	return PluralUtil{r}

}


func (p PluralUtil) ToSingular(plural string) string  {
	for _,rule := range p.RuleGroup.PluralRules{
		from := regexp.MustCompile(rule.From)
		if from.MatchString(plural) {
			return from.ReplaceAllString(plural,rule.To)
		}
	}
	for _,rule := range p.RuleGroup.IrregularRules{
		if plural==rule.To {
			return rule.From
		}
	}
	return  plural

}

func (p PluralUtil) ToPlural(plural string) string  {
	for _,rule := range p.RuleGroup.SingularRules{
		from := regexp.MustCompile(rule.From)
		if from.MatchString(plural) {
			return from.ReplaceAllString(plural,rule.To)
		}
	}
	for _,rule := range p.RuleGroup.IrregularRules{
		if plural==rule.From {
			return rule.To
		}
	}
	return  plural

}