package main

import (
	"regexp"
	"strings"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"os"
	"bufio"
	"text/template"
)

func upCamelCase( name string) string {
	regexSub := regexp.MustCompile("[_-]")
	ret :=regexSub.ReplaceAllString(name," ")
	ret = strings.Title(ret)
	regexES := regexp.MustCompile("\\s")
	ret = regexES.ReplaceAllString(ret,"")
	return ret
}

func maxNameLen(t TableSquema)  int{
	var maxLen=0
	for _,e :=range t.Columns {
		l:= len(e.Name)
		if l>maxLen{
			maxLen=l
		}
	}
	for _,e :=range t.HasMany {
		l:= len(e.SourceTable)
		if l>maxLen{
			maxLen=l
		}
	}
	for _,e :=range t.BelongsTo {
		l:= len(e.TargetTable)
		if l>maxLen{
			maxLen=l
		}
	}

	fmt.Println("max ",maxLen)
	return maxLen
}



func goType(c ColumnSquema) string {
	switch c.DataType {
	case "smallint":return "int16"
	case "integer":return "int"
	case "bigint": return "int64"
	case "real" : return "float32"
	case "double precision","numeric":return "float64"
	case "char","varchar","text","character varying": return "string"
	case "date","time", "timetz", "timestamp", "timestamptz",
		"timestamp without time zone","time without time zone",
		"timestamp with time zone","time with time zone":return "time.Time"
	case "boolean":return "bool"
	case "bytea": return "[]byte"
	default: return "[]byte"
	}
}

func nameLenSpace(name string ,colLen int,extraSpace int) string{
	spaceLen := colLen - len(name) + extraSpace
	fmt.Println(spaceLen)
	return strings.Repeat(" ",spaceLen)
}


func GenCmd(fileUrl string,dbFileUrl string) int {
	fmt.Println("gen command...")

	dbfile, err := ioutil.ReadFile(dbFileUrl)
	if err!=nil {
		fmt.Println(err.Error())
		fmt.Println("run",appName,"init first")
		return 1
	}
	var dbs DbSchema
	err = json.Unmarshal(dbfile, &dbs)
	if(err!=nil){
		fmt.Println(err.Error())
		return 1
	}
	//Create structs
	f, err := os.Create("./model.go2")
	if err != nil {
		fmt.Println(err.Error())
		return 1
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	var pu = NewPluralUtil("en")
	var funcs = make(map[string]interface{})
	funcs["one"] = func() string {return "one func"}
	funcs["maxNameLen"] = maxNameLen
	funcs["goType"] =goType
	funcs["upCamelCase"] = upCamelCase
	funcs["nameLenSpace"]= nameLenSpace
	funcs["toPlural"] =pu.ToPlural
	funcs["toSingular"] = pu.ToSingular
	funcs["indent"] = func() string{return "    "}
	funcs["br"] = func() string{return "\n"}
	structTemplate, _ := template.New("struct").Funcs(funcs).Parse(

`package main

//{{.Name}} Generated
{{range .Tables}}{{$s:= maxNameLen .}}
type {{ toSingular .Name | upCamelCase}} struct{
	{{range .Columns -}}
		{{$n := upCamelCase .Name}}{{$n}} {{nameLenSpace $n $s 2 }} {{goType .}}
	{{end -}}
	{{ if .BelongsTo -}}
		{{range .BelongsTo -}}
			{{$n := toSingular .TargetTable | upCamelCase  }}{{$n}} {{nameLenSpace $n $s 2 }} *{{toSingular .TargetTable | upCamelCase }}{{br}}
		{{- indent -}}
		{{- end -}}

	{{end -}}
	{{if .HasMany -}}
		{{range .HasMany -}}
			{{$n :=  upCamelCase .SourceTable }}{{$n}} {{nameLenSpace .SourceTable  $s 2 }} []*{{toSingular .SourceTable | upCamelCase }}{{br}}
		{{- indent -}}
		{{- end -}}
	{{end }}
}
{{end}}

`)


	structTemplate.Execute(w,dbs)
	if err != nil {
		fmt.Println(err.Error())
		return 1
	}
	w.Flush()

	return 0
}

