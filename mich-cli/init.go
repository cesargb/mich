package main

import (
	"fmt"
	"os"
	"bufio"
	"encoding/json"
)

func InitCmd(url string) int{
	fmt.Println("init command...")
	if url[len(url)-1]!='/' {
		url=url+"/"
	}
	f, err := os.Create(url+"dbschema.json")
	if err != nil {
		fmt.Println(err.Error())
		return 1
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	//n, err := w.WriteString("This is bufferedsss\n")
	////Use Error handling as required
	//fmt.Printf("Wrote %d bytes to buffers\n", n)
	//
	//n, err = w.WriteString("So is thissss\n")
	////Use Error handling as required
	//fmt.Printf("Wrote %d bytes to bufferssss\n", n)
	if db,err:=DBSync(url);err==nil{
		j,_ := json.MarshalIndent(db,"","    ")

		w.Write(j)
	}else {
		fmt.Println(err.Error())
		return 1
	}

	// 'Flush` to ensure all buffered operations have
	// been applied to the underlying writer.
	w.Flush()
	//Comment the flush line above and no data will be written
	return 0
}