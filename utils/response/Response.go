package response

import "net/http"

type EmptyArray = [0]interface{}
type ErrorRes struct {
	Status int `json:"status"`
	Error string `json:"error"`
}
func NewErrorRes(status int,error string) (int,*ErrorRes){
	return status, &ErrorRes{status,error}
}
func NewServerErrorRes() (int,ErrorRes){
	return http.StatusInternalServerError,
		ErrorRes{http.StatusInternalServerError,"Internal server error"}
}
