package router

import (
	"net/http"
	"github.com/go-chi/render"
	"github.com/go-chi/chi"
)

type Context struct {
	Writer http.ResponseWriter
	Request   *http.Request
}
func (ctx Context)Json(status int,body interface{})  {
	render.Status(ctx.Request,status)
	render.JSON(ctx.Writer,ctx.Request,body)
}
func (ctx Context)Write(s string)  {
	ctx.Writer.Write([]byte(s))
}

func (ctx Context) BindJSON(obj interface{}) error {
	return render.DefaultDecoder(ctx.Request,&obj)
}

func (ctx Context) UrlParam(str string) string  {
	return chi.URLParam(ctx.Request,str)
}