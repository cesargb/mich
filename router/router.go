package router

import (
	"net/http"
	"github.com/go-chi/render"
	"strconv"
	"github.com/go-chi/chi"
	"gitlab.com/cesargb/mich/utils/response"
	"strings"
)


type Router struct {
	chi.Router
}
type ContextFunc func(ctx *Context)

func (fn ContextFunc) stdFunc() func(http.ResponseWriter, *http.Request)   {
	return func(writer http.ResponseWriter, request *http.Request) {
		fn(&Context{writer,request})
	}
}

func (r Router)  Get(pattern string, fn ContextFunc) {
	r.Router.Get(pattern,fn.stdFunc())
}
func (r Router)  Post(pattern string, fn ContextFunc) {
	r.Router.Post(pattern,fn.stdFunc())
}
func (r Router)  Delete(pattern string, fn ContextFunc) {
	r.Router.Delete(pattern,fn.stdFunc())
}
func (r Router)  Put(pattern string, fn ContextFunc) {
	r.Router.Put(pattern,fn.stdFunc())
}
func (r Router) Route(pattern string, fn func(r Router)) Router {
	subRouter := Router{NewRouter()}
	if fn != nil {
		fn(subRouter)
	}
	r.Mount(pattern, subRouter)
	return subRouter
}

func (r Router) RestRoute(pattern string ,rest Rest, fn func(r Router)) Router {

	return r.Route(pattern, func(r Router) {
		r.Rest("/",rest)
		fn(r)
	})
}

func (r Router) Rest(pattern string ,rest Rest)  {
	pattern = strings.TrimSpace(pattern)
	for strings.HasSuffix(pattern,"/"){
		pattern = pattern[:len(pattern)-1]
	}
	r.Get(pattern+"/", func(ctx *Context) {
		ctx.Json(rest.GetAll())
	})
	r.Post(pattern+"/", func(ctx *Context) {
		model:=rest.Model()

		err :=render.DefaultDecoder(ctx.Request,model)
		if err!=nil{
			ctx.Json(response.NewErrorRes(http.StatusBadRequest,err.Error()))
			return
		}
		ctx.Json(rest.Create(model))
	})
	r.Get(pattern+"/{id:\\d+}", func(ctx *Context) {
		idStr:=	chi.URLParam(ctx.Request,"id")
		id,err:= strconv.Atoi(idStr)
		if err!=nil{
			ctx.Json(response.NewErrorRes(http.StatusBadRequest,err.Error()))
			return
		}
		ctx.Json(rest.GetById(id))

	})
	r.Put(pattern+"/{id:\\d+}", func(ctx *Context) {
		idStr:=	chi.URLParam(ctx.Request,"id")
		id,err:= strconv.Atoi(idStr)
		if err!=nil{
			ctx.Json(response.NewErrorRes(http.StatusBadRequest,err.Error()))
			return
		}
		model:=rest.Model()
		err =render.DefaultDecoder(ctx.Request,model)
		if err!=nil{
			ctx.Json(response.NewErrorRes(http.StatusBadRequest,err.Error()))
			return
		}
		ctx.Json(rest.Edit(id,model))
	})
	r.Delete(pattern+"/{id:\\d+}", func(ctx *Context) {
		idStr:=	chi.URLParam(ctx.Request,"id")
		id,err:= strconv.Atoi(idStr)
		if err!=nil{
			ctx.Json(response.NewErrorRes(http.StatusBadRequest,err.Error()))
			return
		}
		ctx.Json(rest.DelById(id))
	})
}
