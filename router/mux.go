package router

import "github.com/go-chi/chi"

// NewRouter returns a new Mux object that implements the StdRouter interface.
func NewRouter() *Mux {
	return &Mux{chi.NewRouter()}
}
func NewRouterB(mux *chi.Mux) *Mux {
	return &Mux{mux}
}
func NewRouterC() *chi.Mux {
	return chi.NewRouter()
}
type Mux struct {
	*chi.Mux
}

func (m *Mux) RouterWithCtx(pattern string, fn func(r Router)) Router {
	subRouter := Router{NewRouter()}
	if fn != nil {
		fn(subRouter)
	}
	m.Mount(pattern, subRouter)
	return subRouter
}
