package router

type Rest interface {
	Model() interface{}
	GetAll() (int,interface{})
	GetById(int) (int,interface{})
	Create(interface{}) (int,interface{})
	Edit(int, interface{})  (int,interface{})
	DelById(int) (int,interface{})

}