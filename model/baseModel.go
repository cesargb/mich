package model

import "time"

type BaseModel struct {
	Id *int `json:"id"`
	CreatedAt *time.Time  `json:"created_at"`
	UpdatedAt *time.Time  `json:"updated_at"`
	
}

type Validable interface {
	ValidateCreate() (bool, string)
	ValidateEdit()
}



func (m *BaseModel) ValidateCreate() (bool, string) {
	if m.Id !=nil{
		return false, "id es un valor automatico, no puede ser elegido"
	}
	if m.CreatedAt!=nil {
		return false,"created_at es un valor automatico, no puede ser elegido"
	}
	if m.UpdatedAt!=nil {
		return false,"updated_at es un valor automatico, no puede ser elegido"
	}
	return true,"ok"

}

func (m *BaseModel) ValidateEdit() (bool, string) {
	if m.Id !=nil{
		return false, "id no puede ser cambiado"
	}
	if m.CreatedAt!=nil {
		return false,"created_at es un valor automatico, no puede ser elegido"
	}
	if m.UpdatedAt!=nil {
		return false,"updated_at es un valor automatico, no puede ser elegido"
	}
	return true,"ok"
}