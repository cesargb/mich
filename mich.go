// mich is go-chi + util functions
package mich
import (
	"github.com/go-pg/pg"
	"gitlab.com/cesargb/mich/router"
)
type DBConf struct {
	*pg.Options
}

type AppConf struct {
	DB DBConf
}
type RouterConf struct {
	route string
	router *router.Mux
}


type App struct {
	Routers []RouterConf

}

//
//func NewApp()  {
//	a := DBConf{}.Database
//}
//
//func NewAppWConf()  {
//	a := DBConf{}.Database
//}